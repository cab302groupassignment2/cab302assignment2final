import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.VolatileImage;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import command.Command;
/*
 *
 */
public class Page extends JLabel implements MouseListener, MouseMotionListener, MouseWheelListener {
	
	private String mShapeType = null;
	
	private Color mLineColor = null;
	private Color mFillColor = null;

	private Color mLineColorForDecoding = null;
	private Color mFillColorForDecoding = null;
	
	private ArrayList<Command> mCommands = new ArrayList<>();
	
	private Image mBufferedImage = null;
			
	private Point mMousePressedPoint = null;
	private Point mMouseCurrentPoint = null;
	
	private int mZoom = 0;
	
	private int mOriginSize = 0;
	
	private Polygon mPolygon = null;
	
	private MainFrame mMainFrame = null;

	/*
	 *
	 */
	public Page(MainFrame mainFrame, int originSize) {
		mMainFrame = mainFrame;
		mOriginSize = originSize;
		
		addMouseListener(this);
		addMouseWheelListener(this);
		addMouseMotionListener(this);
		
		mShapeType = "PLOT";
	}

	/*
	 *
	 */
	public void clear() {
		mCommands.clear();

		mMainFrame.endPolygon();
		
		switch (mShapeType) {
		case "POLYGON":
			mPolygon = new Polygon();
			break;
			
		default:
			mPolygon = null;
			break;
		}
		
		repaint();
	}

	public void endPolygon() {
		Command command = new Command("POLYGON");
		
		for (int index = 0 ; index < mPolygon.npoints ; ++index) {
			command.addParameter(mPolygon.xpoints[index] / (double) getWidth());
			command.addParameter(mPolygon.ypoints[index] / (double) getHeight());
		}
		
		mCommands.add(command);
		
		mPolygon = new Polygon();
		mMainFrame.endPolygon();
	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(mOriginSize + mZoom, mOriginSize + mZoom);
	}

	public ArrayList<Command> getCommands() {
		return mCommands;
	}

	public void load(ArrayList<Command> commands) {
		mCommands.addAll(commands);
		
		for (Command command : mCommands) {
			switch (command.getType()) {
			case "PEN":
				mMainFrame.selectColor(ColorSelectorMode.LINE, command.getParameter(0), false);
				break;
				
			case "FILL":
				switch (command.getParameter(0)) {
				case "OFF":
					mMainFrame.selectColor(ColorSelectorMode.FILL, "#TRANS", false);
					break;
					
				default:
					mMainFrame.selectColor(ColorSelectorMode.FILL, command.getParameter(0), false);
					break;
				}
			}
		}
	}

	public void setShape(String shapeType) {
		mShapeType = shapeType;
		
		switch (mShapeType) {
		case "POLYGON":
			mPolygon = new Polygon();
			break;
			
		default:
			mPolygon = null;
			break;
		}
	}

	public void setLineColor(String lineColor, boolean adjust) {
		mLineColor = Color.decode(lineColor);
		
		if (adjust) {
			mCommands.add(new Command("PEN").addParameter(lineColor));
		}
	}

	public void setFillColor(String fillColor, boolean adjust) {
		mFillColor = fillColor.equals("#TRANS") ? null : Color.decode(fillColor);
		
		if (adjust) {
			mCommands.add(new Command("FILL").addParameter(mFillColor != null ? fillColor : "OFF"));
		}
	}

	@Override
	public void paint(Graphics graphics) {
		/*
		 * 	History
		 */
		mBufferedImage = createImage(getWidth(), getHeight());
		
		Graphics2D graphics2dForHistory = (Graphics2D) mBufferedImage.getGraphics();
		
		graphics2dForHistory.setRenderingHint(
				RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
		
		graphics2dForHistory.setColor(Color.WHITE);
		graphics2dForHistory.fillRect(0, 0, getWidth(), getHeight());
		
		mLineColorForDecoding = Color.BLACK;
		mFillColorForDecoding = null;
		
		for (Command command : mCommands) {
			decode(graphics2dForHistory, command);
		}
		graphics.drawImage(mBufferedImage, 0, 0, this);
		
		
		/*
		 * 	Current
		 */
		Graphics2D graphics2dForCurrent = (Graphics2D) graphics;
		
		graphics2dForCurrent.setRenderingHint(
				RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
		
		graphics2dForCurrent.setColor(mLineColor);

		switch (mShapeType) {
		case "PLOT":
			if (mMouseCurrentPoint != null) {
				graphics2dForCurrent.setColor(mLineColor);
				graphics2dForCurrent.drawRect(mMouseCurrentPoint.x, mMouseCurrentPoint.y, 1, 1);
			}
			break;
			
		case "LINE":
			if (mMouseCurrentPoint != null) {
				graphics2dForCurrent.setColor(mLineColor);
				graphics2dForCurrent.drawLine(
						mMousePressedPoint.x, mMousePressedPoint.y,
						mMouseCurrentPoint.x, mMouseCurrentPoint.y);
			}
			break;
			
		case "POLYGON":
			if (mMouseCurrentPoint != null) {
				graphics2dForCurrent.setColor(mLineColor);
				graphics2dForCurrent.drawLine(
						mMousePressedPoint.x, mMousePressedPoint.y,
						mMouseCurrentPoint.x, mMouseCurrentPoint.y);
			}
			
			if (mPolygon != null) {
				if (mFillColor != null) {
					graphics2dForCurrent.setColor(mFillColor);
					graphics2dForCurrent.fillPolygon(mPolygon);
				}
				
				graphics2dForCurrent.setColor(mLineColor);
				graphics2dForCurrent.drawPolygon(mPolygon);
			}
			break;
			
		case "ELLIPSE":
		case "RECTANGLE":
			if (mMouseCurrentPoint != null) {
				int width = mMousePressedPoint.x - mMouseCurrentPoint.x;
				int height = mMousePressedPoint.y - mMouseCurrentPoint.y;

				switch (mShapeType) {
				case "ELLIPSE":
					if (mFillColor != null) {
						graphics2dForCurrent.setColor(mFillColor);
						graphics2dForCurrent.fillOval(
								width < 0 ? mMousePressedPoint.x : mMouseCurrentPoint.x,
										height < 0 ? mMousePressedPoint.y : mMouseCurrentPoint.y,
										Math.abs(width), Math.abs(height));
					}
					
					graphics2dForCurrent.setColor(mLineColor);
					graphics2dForCurrent.drawOval(
							width < 0 ? mMousePressedPoint.x : mMouseCurrentPoint.x,
							height < 0 ? mMousePressedPoint.y : mMouseCurrentPoint.y,
							Math.abs(width), Math.abs(height));
					break;
					
				case "RECTANGLE":
					if (mFillColor != null) {
						graphics2dForCurrent.setColor(mFillColor);
						graphics2dForCurrent.fillRect(
								width < 0 ? mMousePressedPoint.x : mMouseCurrentPoint.x,
								height < 0 ? mMousePressedPoint.y : mMouseCurrentPoint.y,
								Math.abs(width), Math.abs(height));
					}
					
					graphics2dForCurrent.setColor(mLineColor);
					graphics2dForCurrent.drawRect(
							width < 0 ? mMousePressedPoint.x : mMouseCurrentPoint.x,
							height < 0 ? mMousePressedPoint.y : mMouseCurrentPoint.y,
							Math.abs(width), Math.abs(height));
					break;
				}
			}
			break;
		}
	}
	
	private void decode(Graphics2D graphics2d, Command command) {
		switch (command.getType()) {
		case "PEN":
			mLineColorForDecoding = Color.decode(command.getParameter(0));
			break;
			
		case "FILL": {
			mFillColorForDecoding = command.getParameter(0).equals("OFF") ?
					null : Color.decode(command.getParameter(0));
		} break;
			
		case "PLOT":
			graphics2d.setColor(mLineColorForDecoding);
			graphics2d.drawRect(
					(int) (getWidth() * command.getParameterAsDouble(0)),
					(int) (getHeight() * command.getParameterAsDouble(1)),
					1, 1);
			break;
			
		case "LINE":
			graphics2d.setColor(mLineColorForDecoding);
			graphics2d.drawLine(
					(int) (getWidth() * command.getParameterAsDouble(0)),
					(int) (getHeight() * command.getParameterAsDouble(1)),
					
					(int) (getWidth() * command.getParameterAsDouble(2)),
					(int) (getHeight() * command.getParameterAsDouble(3)));
			break;
			
		case "ELLIPSE":
			if (mFillColorForDecoding != null) {
				graphics2d.setColor(mFillColorForDecoding);
				graphics2d.fillOval(
						(int) (getWidth() * command.getParameterAsDouble(0)),
						(int) (getHeight() * command.getParameterAsDouble(1)),
						
						(int) (getWidth() * command.getParameterAsDouble(2)),
						(int) (getHeight() * command.getParameterAsDouble(3)));
			}
			
			graphics2d.setColor(mLineColorForDecoding);
			graphics2d.drawOval(
					(int) (getWidth() * command.getParameterAsDouble(0)),
					(int) (getHeight() * command.getParameterAsDouble(1)),
					
					(int) (getWidth() * command.getParameterAsDouble(2)),
					(int) (getHeight() * command.getParameterAsDouble(3)));
			break;
			
		case "RECTANGLE":
			if (mFillColorForDecoding != null) {
				graphics2d.setColor(mFillColorForDecoding);
				graphics2d.fillRect(
						(int) (getWidth() * command.getParameterAsDouble(0)),
						(int) (getHeight() * command.getParameterAsDouble(1)),
						
						(int) (getWidth() * command.getParameterAsDouble(2)),
						(int) (getHeight() * command.getParameterAsDouble(3)));
			}
			
			graphics2d.setColor(mLineColorForDecoding);
			graphics2d.drawRect(
					(int) (getWidth() * command.getParameterAsDouble(0)),
					(int) (getHeight() * command.getParameterAsDouble(1)),
					
					(int) (getWidth() * command.getParameterAsDouble(2)),
					(int) (getHeight() * command.getParameterAsDouble(3)));
			break;
			
		case "POLYGON":
			Polygon polygon = new Polygon();
			int size = command.getSizeOfParameters();
			
			for (int index = 0 ; index < size ; index += 2) {
				polygon.addPoint(
						(int) (getWidth() * command.getParameterAsDouble(index)),
						(int) (getHeight() * command.getParameterAsDouble(index + 1)));
			}
			
			if (mFillColorForDecoding != null) {
				graphics2d.setColor(mFillColorForDecoding);
				graphics2d.fillPolygon(polygon);
			}
			
			graphics2d.setColor(mLineColorForDecoding);
			graphics2d.drawPolygon(polygon);
			break;
		}
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		mMousePressedPoint = e.getPoint();
		mMouseCurrentPoint = e.getPoint();

		switch (mShapeType) {
		case "POLYGON":
			mPolygon.addPoint(mMousePressedPoint.x, mMousePressedPoint.y);
			break;
			
		default:
			break;
		}
		
		repaint();
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (mMousePressedPoint != null) {
			mMouseCurrentPoint = e.getPoint();
			repaint();
		}
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		if (mMouseCurrentPoint != null) {
			switch (mShapeType) {
			case "PLOT":
				mCommands.add(
						new Command(mShapeType)
							.addParameter(mMouseCurrentPoint.getX() / (double) getWidth())
							.addParameter(mMouseCurrentPoint.getY() / (double) getHeight()));
				break;
				
			case "LINE":
				mCommands.add(
						new Command(mShapeType)
							.addParameter(mMousePressedPoint.getX() / (double) getWidth())
							.addParameter(mMousePressedPoint.getY() / (double) getHeight())
							.addParameter(mMouseCurrentPoint.getX() / (double) getWidth())
							.addParameter(mMouseCurrentPoint.getY() / (double) getHeight()));
				break;
				
			case "ELLIPSE":
			case "RECTANGLE":
				double x0 = mMousePressedPoint.getX() / (double) getWidth();
				double x1 = mMouseCurrentPoint.getX() / (double) getHeight();
				
				double y0 = mMousePressedPoint.getY() / (double) getWidth();
				double y1 = mMouseCurrentPoint.getY() / (double) getHeight();
				
				mCommands.add(
						new Command(mShapeType)
							.addParameter(x0 < x1 ? x0 : x1)
							.addParameter(y0 < y1 ? y0 : y1)
							.addParameter(Math.abs(x0 - x1))
							.addParameter(Math.abs(y0 - y1)));
				break;
				
			case "POLYGON":
				switch (mPolygon.npoints) {
				case 1:
					mMainFrame.beginPolygon();
					break;
					
				default:
					break;
				}
				mPolygon.addPoint(mMouseCurrentPoint.x, mMouseCurrentPoint.y);
				repaint();
				break;
			}
		}
		
		mMousePressedPoint = null;
		mMouseCurrentPoint = null;
		repaint();
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}
	
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		if (e.isControlDown()) {
			// up minus, down plus
			switch (mShapeType) {
			case "POLYGON":
				break;
				
			default:
				mMainFrame.zoom(mZoom + (e.getWheelRotation() * -10));
			}
		} else {
			getParent().getParent().getParent().getMouseWheelListeners()[0].mouseWheelMoved(e);
		}
	}

	public void undo() {
		if (mCommands.size() > 0) {
			mCommands.remove(mCommands.size() - 1);
			repaint();
		}
	}

	public void zoom(int zoom) {
		mZoom = zoom;
		repaint();
	}
}
