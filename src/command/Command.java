package command;

import java.util.ArrayList;

/*
 *
 */
public class Command {
	
	private String mType = "";
	private ArrayList<String> mParameters = new ArrayList<>();
	
	public Command(String type) {
		mType = type;
	}
	
	public Command(String[] tokens) {
		mType = tokens[0];
		
		for (int index = 1 ; index < tokens.length ; ++index) {
			mParameters.add(tokens[index]);
		}
	}
	/*
	 *
	 */
	public void addParameters(String[] tokens) {
		for (int index = 0 ; index < tokens.length ; ++index) {
			mParameters.add(tokens[index]);
		}
	}
	
	public String getType() {
		return mType;
	}
	
	public ArrayList<String> getParameters() {
		return mParameters;
	}
	
	public String getParameter(int index) {
		return mParameters.get(index);
	}
	
	public double getParameterAsDouble(int index) {
		return Double.parseDouble(mParameters.get(index));
	}
	
	public Command addParameter(double value) {
		return addParameter(value + "");
	}
	
	public Command addParameter(String value) {
		mParameters.add(value);
		return this;
	}
	
	@Override
	/*
	 *
	 */
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		
		buffer.append(mType);
		for (String parameter : mParameters) {
			buffer.append(" " + parameter);
		}
		
		return buffer.toString();
	}

	public int getSizeOfParameters() {
		return mParameters.size();
	}
}
