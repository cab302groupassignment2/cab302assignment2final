import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;

import command.Command;

public class MainFrame extends JFrame implements ActionListener, ChangeListener {
	/*
	 * Declared part, mCurrentFilePath: Set to the path string you want to access now.File Menu
	 */
	public static final int ZOOM_MIN = 0;
	public static final int ZOOM_MAX = 1000;

	private String mCurrentFilePath = null;
	/*
	 * Create declaration: page, cover, color panel, zoom part, polygon, line color
	 * fill color button, screen dimension
	 */
	private Page mPage = null;
	private JLabel mCover = null;
	private JPanel mColorPanel = null;

	private JLabel mZoomLabel = null;
	private JPanel mZoomPane = null;
	private JSlider mZoomSlider = null;
	private JPanel mPolygonPane = null;

	private JButton mLineColorButton = null;
	private JButton mFillColorButton = null;

	private Dimension mScreenDimension = null;
	/*
	 * Declare to store integer, eum value
	 */
	private static final int LINE = 0;
	private static final int FILL = 0;
	private ColorSelectorMode mColorSelectorMode = ColorSelectorMode.LINE;
	/*
	 * Create a method for the mainframe and set the title
	 */
	public MainFrame() {
	    /*
		 * Bring the function Set title to another library and
		 * insert "VEC Editor - New". setTitle("VEC Editor - New")
		 */


		/*
		 * Creates a CloseOperation.
		 */
		setDefaultCloseOperation(EXIT_ON_CLOSE);
        /*
         * Determine the overall screen size to draw: the default we want to get
         * from the toolkit class, the size of the screen we want to get
         */
		mScreenDimension = Toolkit.getDefaultToolkit().getScreenSize();

		int width = (int) (mScreenDimension.getWidth() * 0.6f);
		int height = (int) (mScreenDimension.getHeight() * 0.8f);
         /*
         * This is a function with two integers as parameters, role?
         */
         setLocation(
				(int) ((mScreenDimension.getWidth() - width) / 2),
				(int) ((mScreenDimension.getHeight() - height) / 2));
         /*
          * Enables resizing (true only)
          */
		setResizable(true);
//      setAlwaysOnTop(true);
         /*
         * Set the width and length.
         */
		setSize(width, height);
		/*
         * Declare a new class and write a new layout
		 * A new layout is required to run the screen.
         */
		setLayout(new BorderLayout());


		/*
		 * Menu, ContentPanel, ColorPanel
		 */
		initializeMenu();
		initializeContentPanel();
		initializeColorPanel();
		/*
         * An initialization process that determines the color for the fill and line
         */
		selectColor(ColorSelectorMode.FILL, "#TRANS", false);
		selectColor(ColorSelectorMode.LINE, "#000000", false);
		/*
		 * small cover: Declare J-label. (J-label: library), we must pass the image icon as a parameter.
		 * Create an image icon. (Image icon: library), we must pass a string to the image icon: the file path.
         * Go back one step (src folder vec edit, move to res folder, loading.gif image).
         */
		mCover = new JLabel(new ImageIcon("./res/loading.gif"));
		/*
		 * The background color determines what to do: white
         */
		mCover.setBackground(Color.WHITE);
		/*
		 * Make the cover color opaque.
		 */
		mCover.setOpaque(true);
		/*
		// Glass Pane
		 */
		setGlassPane(mCover);

//      getGlassPane().setVisible(true);
	}

	/*
	 * menu Item:
	 */
	private JMenuItem addMenuItem(JMenu menu, String command) {
		JMenuItem menuItem = new JMenuItem(command);

		menuItem.addActionListener(this);

		menu.add(menuItem);

		return menuItem;
	}
	private void addSeparator(JMenu menu) {
		menu.addSeparator();
	}
	/*
	 * Initialize it and put it in the menu bar space.
	 */
	private void initializeMenu() {
		JMenuBar menuBar = new JMenuBar();
		/*
         * Fault : The default value is a false
         */
		JPopupMenu.setDefaultLightWeightPopupEnabled(false);

		/*
		 * File Menu
		 */

		/*
		*At J-Menu, the function with the same name has parameters.
		*Send a function called file.
		*/
		JMenu fileMenu = new JMenu("File");
		addMenuItem(fileMenu, "New");
		/*
		 * Separate the menus from the File menu.
		 * (New / Save, Load / Exit)
		 */
		addSeparator(fileMenu);
		addMenuItem(fileMenu, "Save");
		addMenuItem(fileMenu, "Load");
		addSeparator(fileMenu);
		addMenuItem(fileMenu, "Exit");
		menuBar.add(fileMenu);

		/*
		 * Shape Menu
		 */
		JMenu shapeMenu = new JMenu("Shape");
		/*
		 * Shape menu by type.
		 */
		addMenuItem(shapeMenu, "PLOT");
		addMenuItem(shapeMenu, "LINE");
		addMenuItem(shapeMenu, "ELLIPSE");
		addMenuItem(shapeMenu, "POLYGON");
		addMenuItem(shapeMenu, "RECTANGLE");
		menuBar.add(shapeMenu);
        /*
         * Edit Menu
         */
		JMenu editMenu = new JMenu("Edit");
		/*
		 * Put undo in edit.
		 * (Add menu: Returns the menu item, and returns the undo item)
		 */
		JMenuItem undoMenuItem = addMenuItem(editMenu, "Undo");
		/*
		 * An Accelerator is a mapping between a particular keyboard combination
		 * and a command. When you press the combination of keys on the keyboard, the command is executed.
		 */
		undoMenuItem.setAccelerator(
				KeyStroke.getKeyStroke(
						KeyEvent.VK_Z,
						Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		menuBar.add(editMenu);
		/*
		 * Set menu bar to mainframe.
		 */
		setJMenuBar(menuBar);
	}
/*
 * Initialize the Color Panel
 */
	private void initializeColorPanel() {
		int unitSize = mScreenDimension.height / 16;

		/*
		 * Color Panel
		 */
		mColorPanel = new JPanel();
		mColorPanel.setPreferredSize(
				new Dimension(
						mScreenDimension.width,
						unitSize));
		mColorPanel.setLayout(new BorderLayout());

		/*
		 * Selected Panel
		 */
		JPanel selectedPanel = new JPanel();
		selectedPanel.setPreferredSize(
				new Dimension(
						unitSize * 2,
						unitSize));
		selectedPanel.setLayout(new GridLayout(1, 2));
		selectedPanel.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, Color.GRAY));

		JPanel lineColorPanel = new JPanel();
		JPanel fillColorPanel = new JPanel();

		int padding = unitSize / 10;

		lineColorPanel.setLayout(new BorderLayout());
		fillColorPanel.setLayout(new BorderLayout());

		lineColorPanel.setBorder(BorderFactory.createEmptyBorder(padding, padding, padding, padding / 2));
		fillColorPanel.setBorder(BorderFactory.createEmptyBorder(padding, padding/ 2, padding, padding));

		mLineColorButton = new JButton("") {
			@Override
			public Dimension getPreferredSize() {
				Dimension dimension = super.getPreferredSize();

				dimension.height = (int) (dimension.height * 0.7f);

				return dimension;
			}
		};
		/*
		 *
		 */
		mFillColorButton = new JButton("") {
			@Override
			public Dimension getPreferredSize() {
				Dimension dimension = super.getPreferredSize();

				dimension.height = (int) (dimension.height * 0.7f);

				return dimension;
			}
		};
		/*
		 * Set the Action Command When user select Line Color Button or Fill color Button
		 */
		mLineColorButton.setActionCommand("LINE_COLOR");
		mFillColorButton.setActionCommand("FILL_COLOR");

		mLineColorButton.addActionListener(this);
		mFillColorButton.addActionListener(this);

		lineColorPanel.add(mLineColorButton, BorderLayout.CENTER);
		fillColorPanel.add(mFillColorButton, BorderLayout.CENTER);

		lineColorPanel.add(new JLabel("Outline", SwingConstants.CENTER), BorderLayout.SOUTH);
		fillColorPanel.add(new JLabel("Fill", SwingConstants.CENTER), BorderLayout.SOUTH);

		selectedPanel.add(lineColorPanel);
		selectedPanel.add(fillColorPanel);

		mColorPanel.add(selectedPanel, BorderLayout.WEST);

		/*
		 * Piece Panel
		 */
		JPanel piecePanel = new JPanel();
		piecePanel.setLayout(new GridLayout(2, 1));

		JPanel pieceTopPanel = new JPanel();
		JPanel pieceBottomPanel = new JPanel();

		pieceTopPanel.setBorder(BorderFactory.createEmptyBorder());

		pieceTopPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
		pieceBottomPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));

		pieceTopPanel.add(makeColorButton("#FF0000", unitSize));
		pieceTopPanel.add(makeColorButton("#FF7F00", unitSize));
		pieceTopPanel.add(makeColorButton("#FFFF00", unitSize));
		pieceTopPanel.add(makeColorButton("#00FF00", unitSize));
		pieceTopPanel.add(makeColorButton("#0000FF", unitSize));

		pieceBottomPanel.add(makeColorButton("#4B0082", unitSize));
		pieceBottomPanel.add(makeColorButton("#947FD3", unitSize));
		pieceBottomPanel.add(makeColorButton("#000000", unitSize));
		pieceBottomPanel.add(makeColorButton("#FFFFFF", unitSize));
		pieceBottomPanel.add(makeColorButton("#TRANS", unitSize));

		piecePanel.add(pieceTopPanel);
		piecePanel.add(pieceBottomPanel);

		mColorPanel.add(piecePanel, BorderLayout.CENTER);

		JPanel optionPanel = new JPanel();
		optionPanel.setLayout(new GridLayout(2, 1, 0, 0));
		optionPanel.setPreferredSize(
				new Dimension(
						unitSize * 3,
						unitSize));

		/*
		 * Polygon
		 */
		mPolygonPane = new JPanel();
//      mPolygonPane.setPreferredSize(
//            new Dimension(
//                  unitSize * 2,
//                  unitSize));
		mPolygonPane.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.GRAY));
		mPolygonPane.setLayout(new BorderLayout());

		JButton endPolygonButton = new JButton("End Polygon");
		endPolygonButton.setActionCommand("EndPolygon");
		endPolygonButton.addActionListener(this);
		mPolygonPane.add(endPolygonButton);

		optionPanel.add(mPolygonPane);
		mPolygonPane.setVisible(false);

		/*
		 * Zoom
		 */
		mZoomPane = new JPanel();
		mZoomPane.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));
		mZoomPane.setLayout(new BorderLayout());
		optionPanel.add(mZoomPane);

		mZoomSlider = new JSlider(ZOOM_MIN, ZOOM_MAX, 0);
		mZoomSlider.addChangeListener(this);
		mZoomPane.add(mZoomSlider, BorderLayout.CENTER);

		mZoomLabel = new JLabel("0", SwingConstants.CENTER);
		mZoomPane.add(mZoomLabel, BorderLayout.EAST);

		mColorPanel.add(optionPanel, BorderLayout.EAST);

		add(mColorPanel, BorderLayout.SOUTH);
	}
	/*
	 * Make the Color Button
	 */
	private JPanel makeColorButton(String color, int unitSize) {
		JPanel wrapper = new JPanel();
		wrapper.setPreferredSize(new Dimension(unitSize / 2, unitSize / 2));

		int padding = unitSize / 16;
		wrapper.setLayout(new BorderLayout());
		wrapper.setBorder(BorderFactory.createEmptyBorder(padding, padding, padding, padding));

		JButton button = new JButton();
		button.setActionCommand(color);
		button.addActionListener(this);
		button.setSize(unitSize, unitSize);
		switch (color) {
			case "#TRANS":
				try {
					button.setIcon(
							new ImageIcon(
									ImageIO.read(new File("res/transparent.jpg"))));
				} catch (Exception e) {
					button.setBackground(Color.decode("#FFFFFF"));
				}
				break;

			default:
				button.setBackground(Color.decode(color));
		}

		wrapper.add(button, BorderLayout.CENTER);

		return wrapper;
	}

	private JPanel mContentPane = null;
	private JScrollPane mContentScrollPane = null;
	private void initializeContentPanel() {
		int pageSize = (int) (getHeight() * 0.75f);

		mContentPane = new JPanel();
		mContentPane.setBackground(Color.LIGHT_GRAY);
		mContentPane.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));

		mPage = new Page(this, pageSize);

		mContentPane.add(mPage);
		mContentScrollPane = new JScrollPane(mContentPane);

		add(mContentScrollPane, BorderLayout.CENTER);
	}

//   public void zoom() {
//      mContentPane.doLayout();
//      mContentScrollPane.validate();
//   }
	/*
	 * Zoom
	 */
	public void zoom(int value) {
		if       (value < ZOOM_MIN)   value = ZOOM_MIN;
		else if (value > ZOOM_MAX)   value = ZOOM_MAX;
		else                  ;

		if (mZoomSlider.getValue() != value) {
			mZoomSlider.setValue(value);
		}
	}
	/*
	 * Select Color (When user select button : Line, Fill
	 */
	public void selectColor(
			ColorSelectorMode colorSelectorMode,
			String color,
			boolean adjust) {

		final JButton button;
		switch (colorSelectorMode) {
			case LINE:
				if (color.equals("#TRANS")) {
					return;
				} else {
					button = mLineColorButton;
					mPage.setLineColor(color, adjust);
				}
				break;

			default:
			case FILL:
				button = mFillColorButton;
				mPage.setFillColor(color, adjust);
				break;
		}

		switch (color) {
			case "#TRANS":
				try {
					button.setIcon(
							new ImageIcon(
									ImageIO.read(new File("res/transpalrent.jpg"))));
				} catch (Exception e) {
					button.setBackground(Color.decode("#FFFFFF"));
				}
				break;

			default:
				button.setIcon(null);
				button.setBackground(Color.decode(color));
				break;
		}
	}

	@Override
	/*
	 * Formed When User Action : New, Exit, Save, Load, Edit(Undo))
	 */
	public void actionPerformed(ActionEvent event) {
		String command = event.getActionCommand();

		if (command.startsWith("#")) {
			selectColor(mColorSelectorMode, command, true);
		} else {
			switch (command) {
				case "New":
					selectColor(ColorSelectorMode.FILL, "#TRANS", false);
					selectColor(ColorSelectorMode.LINE, "#000000", false);
					setTitle("VEC Editor - New");
					mCurrentFilePath = null;
					mPage.clear();
					break;

				case "Exit":
					System.exit(0);
					break;

				case "Save": {
					getGlassPane().setVisible(true);

					if (mCurrentFilePath == null) {
						boolean continueFileChooser = true;
						while (continueFileChooser) {
							JFileChooser fileChooser = new JFileChooser(".");

							fileChooser.setAcceptAllFileFilterUsed(false);
							fileChooser.addChoosableFileFilter(new FileFilter() {
								@Override
								/*
								 *
								 */
								public String getDescription() {
									return "VEC (*.vec)";
								}

								@Override
								/*
								 *
								 */
								public boolean accept(File file) {
									return file.isDirectory() || file.getName().endsWith(".vec");
								}
							});

							switch (fileChooser.showSaveDialog(MainFrame.this)) {
								case JFileChooser.APPROVE_OPTION:
									if (fileChooser.getSelectedFile().getName().endsWith(".vec")) {
										mCurrentFilePath = fileChooser.getSelectedFile().getPath();
										setTitle("VEC Editor - " + mCurrentFilePath);
										continueFileChooser = false;
									} else {
										JOptionPane.showMessageDialog(
												this,
												"set file name as *.vec format",
												"Save",
												JOptionPane.ERROR_MESSAGE);
									}
									break;

								default:
									getGlassPane().setVisible(false);
									return;
							}
						}
					}

					FileWriter writer = null;

					try {
						writer = new FileWriter(new File(mCurrentFilePath));
					} catch (IOException e) {
						JOptionPane.showMessageDialog(
								this,
								"Can't create the FileWriter",
								"Save",
								JOptionPane.ERROR_MESSAGE);

						getGlassPane().setVisible(false);
						return;
					}

					for (Command cmd : mPage.getCommands()) {
						try {
							writer.write(cmd.toString() + System.lineSeparator());
						} catch (IOException e) {
//                  e.printStackTrace();
						}
					}

					try {
						writer.close();
					} catch (IOException e) {
						e.printStackTrace();
					} finally {
						getGlassPane().setVisible(false);
					}
				} break;

				case "Load": {
					getGlassPane().setVisible(true);

					JFileChooser fileChooser = new JFileChooser(".");

					fileChooser.setAcceptAllFileFilterUsed(false);
					fileChooser.addChoosableFileFilter(new FileFilter() {
						@Override
						/*
						 *
						 */
						public String getDescription() {
							return "VEC (*.vec)";
						}

						@Override
						public boolean accept(File file) {
							return file.isDirectory() || file.getName().endsWith(".vec");
						}
					});

					switch (fileChooser.showOpenDialog(MainFrame.this)) {
						case JFileChooser.APPROVE_OPTION:

							if (fileChooser.getSelectedFile().getName().endsWith(".vec")) {

								BufferedReader reader = null;
								ArrayList<Command> commands = new ArrayList<>();

								try {
									reader = new BufferedReader(new FileReader(fileChooser.getSelectedFile()));
									mCurrentFilePath = fileChooser.getSelectedFile().getPath();
									setTitle("VEC Editor - " + mCurrentFilePath);
								} catch (FileNotFoundException e1) {
									JOptionPane.showMessageDialog(
											this,
											"*Can't open the file",
											"Load",
											JOptionPane.ERROR_MESSAGE);

									getGlassPane().setVisible(false);
								}

								try {
									String line = null;
									Command polygonCommand = null;
									while ((line = reader.readLine()) != null) {

										String[] tokens = line.split(" ");

										if (polygonCommand != null) {
											if (tokens[0].equals("PEN") ||
													tokens[0].equals("FILL") ||
													tokens[0].equals("PLOT") ||
													tokens[0].equals("LINE") ||
													tokens[0].equals("ELLIPSE") ||
													tokens[0].equals("POLYGON") ||
													tokens[0].equals("RECTANGLE")) {

												commands.add(new Command(tokens));
											} else {
												polygonCommand.addParameters(tokens);
											}
										} else {
											if (tokens[0].equals("POLYGON")) {
												commands.add(polygonCommand = new Command(tokens));
											} else {
												commands.add(new Command(tokens));
											}
										}

									}

									mPage.clear();
									mPage.load(commands);
								} catch (IOException e) {
									JOptionPane.showMessageDialog(
											this,
											"*Can't read the file",
											"Load",
											JOptionPane.ERROR_MESSAGE);
								} finally {
									try {
										reader.close();
									} catch (IOException e) {
										e.printStackTrace();
									}

									getGlassPane().setVisible(false);
								}
							} else {
								JOptionPane.showMessageDialog(
										this,
										"*Load .vec File",
										"Load",
										JOptionPane.ERROR_MESSAGE);
							}
							break;
					}
				} break;

				case "Undo":
					mPage.undo();
					break;

				case "EndPolygon":
					mPage.endPolygon();
					break;

				case "LINE_COLOR":
					mColorSelectorMode = ColorSelectorMode.LINE;
					break;

				case "FILL_COLOR":
					mColorSelectorMode = ColorSelectorMode.FILL;
					break;

				case "PLOT":
				case "LINE":
				case "ELLIPSE":
				case "RECTANGLE":
				case "POLYGON":
					mPage.setShape(command);
					mPage.repaint();
					break;
			}
		}
	}
	/*
	 * The functions to execute are: main function
	 */
	public static void main(final String[] argv) {
		/*
		 * Declare a class called mainframe, call mainframe
		 * and put the value.
		 */
		MainFrame mainFrame = new MainFrame();
		/*
		 * Make mainframe visible by setvisible true.
		 * The mainframe will appear in my window even if we keep still.
		 */
		mainFrame.setVisible(true);
	}
		/*
		 * Polygon to begin with set visible.
		 */
	public void beginPolygon() {
		mZoomPane.setVisible(false);
		mPolygonPane.setVisible(true);
	}
		/*
		 * Polygon to end with set visible.
		 */
	public void endPolygon() {
		mZoomPane.setVisible(true);
		mPolygonPane.setVisible(false);
	}

	@Override
	/*
	 * Zoom Slider. can user zoom in on the image and scroll around
	 */
	public void stateChanged(ChangeEvent event) {
		mPage.zoom(mZoomSlider.getValue());
		mZoomLabel.setText(mZoomSlider.getValue() + "");

		mContentPane.doLayout();
		mContentScrollPane.validate();
	}
}
