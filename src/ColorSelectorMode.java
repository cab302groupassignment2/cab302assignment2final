/*
 * Currently selected color: background or line, outline or fill
 */
public enum ColorSelectorMode {
	LINE,
	FILL;
}